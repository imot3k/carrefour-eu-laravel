<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale()], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get(LaravelLocalization::transRoute('routes.recipes'), 'RecipeController@index')->name('recipes');

    Route::get('search', 'SearchController@index')->name('search');

    Route::group(['prefix' => 'ajax', 'middleware' => 'ajax'], function() {
        Route::get('api-data', 'ApiController@getAsyncData');
    });

    Route::get('{any}', 'RouteController@index')->where('any', '(.*)')->name('any');
});
