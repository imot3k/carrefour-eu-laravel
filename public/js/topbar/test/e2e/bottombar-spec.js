'use strict';

var DemoPage = function() {
  this.get = function() {
    browser.get('/');
  };
};

describe('cfrbottombar', function() {
  var demoPage = new DemoPage();

  beforeEach(function() {
    demoPage.get();
  });

  it('should load', function() {
    expect(element(by.css('.cfrbottombar')).isPresent()).toBe(true);
  });
});
