'use strict';

var DemoPage = function() {
  this.get = function() {
    browser.get('/');
  };
};

describe('cfrbars', function() {
  var demoPage = new DemoPage();

  beforeEach(function() {
    demoPage.get();
  });

  it('should load', function() {
    expect(element(by.css('.cfrbars')).isPresent()).toBe(true);
  });
});
