'use strict';

var DemoPage = function() {
	this.get = function() {
		browser.get('/');
	};
};

describe('cfrtopbar', function() {
	var demoPage = new DemoPage();

	beforeEach(function() {
		demoPage.get();
	});

	it('should load', function() {
		expect(element(by.css('.cfrtopbar')).isPresent()).toBe(true);
	});
});
