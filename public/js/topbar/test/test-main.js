'use strict';

var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

var pathToModule = function(path) {
	return path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function(file) {
	if (TEST_REGEXP.test(file)) {
		// Normalize paths to RequireJS module names.
		allTestFiles.push(pathToModule(file));
	}
});

require.config({
	baseUrl: 'scripts',

	paths: {
		'angular': '../lib/bower/angular/angular',

		'ngSanitize': '../lib/bower/angular-sanitize/angular-sanitize',

		'ngCookies': '../lib/bower/angular-cookies/angular-cookies',

		'angularCSS': '../lib/bower/angular-css/angular-css',

		'clickOut': '../lib/bower/angular-clickout/angular-clickout',

		'underscore': '../lib/bower/lodash/lodash',

		'log': 'logging/console-logger',

		'text': '../lib/bower/requirejs-text/text',

		'components': '../components',

		'cfrbars': '../components/cfrbars/cfrbars',

		'cfrtopbar': '../components/cfrtopbar/main/cfrtopbar',

		'TopbarCtrl': '../components/cfrtopbar/main/TopbarCtrl',

		'cfrtopbarMenu': '../components/cfrtopbar/menu/cfrtopbar-menu',

		'cfrtopbarMenuitem': '../components/cfrtopbar/menuitem/cfrtopbar-menuitem',

		'cfrtopbarMenuitemCtrl': '../components/cfrtopbar/menuitem/cfrtopbar-MenuitemCtrl',

		'cfrtopbarLink': '../components/cfrtopbar/link/cfrtopbar-link',

		'cfrbottombar': '../components/cfrbottombar/main/cfrbottombar',

		'BottombarCtrl': '../components/cfrbottombar/main/BottombarCtrl',

		'cfrbottombarBlock': '../components/cfrbottombar/block/cfrbottombar-block',

		'cfrcookiebar': '../components/cfrcookiebar/main/cfrcookiebar',

		'CookieCtrl': '../components/cfrcookiebar/main/CookieCtrl'
	},

	shim: {
		'angular': {
			exports: 'angular'
		},
		'angular.route': ['angular'],
		'ngSanitize': ['angular'],
		'ngCookies': ['angular'],
		'angularCSS': ['angular'],
		'clickOut': ['angular']
	},

	// dynamically load all test files
	deps: allTestFiles,

	// we have to kickoff jasmine, as it is asynchronous
	callback: window.__karma__.start
});
