var translations_nl = {
  WELCOME_MESSAGE: 'Dag',
  LOGIN_TITLE: 'Meld je aan',
  LOGIN_TEXT_FIRST: 'Handig en eenvoudig.',
  LOGIN_TEXT_SECOND: 'Profiteer van alle voordelen van je Carrefour Account',
  LOGIN_BUTTON: 'Aanmelden',
  REGISTER_TITLE: 'Maak een Account aan',
  REGISTER_TEXT_FIRST: 'Nog niet geregistreerd?',
  REGISTER_TEXT_SECOND: 'Activeer snel je profiel.',
  REGISTER_BUTTON: 'Maak een Account aan',
  LOGIN_MENU_LINK: 'Aanmelden',
  DEFAULT_COOKIE_ACCEPT_LABEL: 'Aanvaarden',
  POINTS: 'pten',
  POINT: 'pt',
  LOGOUT_BUTTON: 'Afmelden'
};