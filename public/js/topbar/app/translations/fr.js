var translations_fr = {
  WELCOME_MESSAGE: 'Bonjour',
  LOGIN_TITLE: 'Connectez-vous',
  LOGIN_TEXT_FIRST: 'Pratique et facile.',
  LOGIN_TEXT_SECOND : 'Profitez de tous les avantages liés à votre Compte Carrefour.',
  LOGIN_BUTTON: 'Connexion',
  REGISTER_TITLE: 'Créez votre Compte',
  REGISTER_TEXT_FIRST: 'Pas encore enregistré?',
  REGISTER_TEXT_SECOND: 'Activez vite votre profil!',
  REGISTER_BUTTON: 'Créez votre Compte',
  LOGIN_MENU_LINK: 'Se connecter',
  DEFAULT_COOKIE_ACCEPT_LABEL: 'Accepter',
  POINTS: 'pts',
  POINT: 'pt',
  LOGOUT_BUTTON: 'Déconnexion'
};