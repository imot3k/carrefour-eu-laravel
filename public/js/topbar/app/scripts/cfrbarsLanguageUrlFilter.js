define(['cfrbarsLanguageUrlFilter'], function() {

  'use strict';

  var cfrbarsLanguageUrlFilter = function(CURRENT_LANGUAGE) {
    return function(text) {
      return String(text).replace('{language}', CURRENT_LANGUAGE);
    };
  };

  cfrbarsLanguageUrlFilter.id = 'cfrbarsLanguageUrl';
  cfrbarsLanguageUrlFilter.$inject = ['CURRENT_LANGUAGE'];

  return cfrbarsLanguageUrlFilter;

});
