define(['angular', 'pascalprecht.translate'], function(angular) {

  'use strict';

  var MainCtrl = function(
    $scope, $http, $sce, cfrbarsService, cfrbarsData, topbarConfig, START_MARGIN_TOP, START_PADDING_TOP
  ) {

    $scope.loaded = false;
    $scope.user = null;
    $scope.hasFooterBlocks = false;
    $scope.hasFooter = false;
    $scope.hasCookieMessage = false;
    $scope.data = {};
    $scope.paddingTop = START_MARGIN_TOP + START_PADDING_TOP;

    if (topbarConfig.userDataLocation) {
      cfrbarsService.loadCurrentUser().then(function() {
        $scope.user = cfrbarsData.user;
        loadSiteData();
      }, function() {
        loadSiteData();
      });
    } else if (location) {
      loadSiteData();
    }

    /**
     * Load the site data and bind needed data.
     */
    function loadSiteData() {

      // Grab general data.
      cfrbarsService.loadSiteData().then(function() {

        if (cfrbarsData.siteData.cookie) {
          $scope.hasCookieMessage = true;
        }

        if (cfrbarsData.siteData.footerbottom) {
          $scope.data.footerbottom = cfrbarsData.siteData.footerbottom;
        }

        $scope.loaded = true;

        var body = angular.element(document).find('body');
        var classes = body[0].getAttribute('class');
        if (classes) {
          classes += ' topbar-active';
        } else {
          classes = 'topbar-active';
        }
        body[0].setAttribute('class', classes);

      });

    }

  };

  MainCtrl.$inject = [
    '$scope',
    '$http',
    '$sce',
    'cfrbarsService',
    'cfrbarsDataFactory',
    'topbarConfig',
    'START_MARGIN_TOP',
    'START_PADDING_TOP'
  ];

  MainCtrl.id = 'MainCtrl';

  return MainCtrl;
});
