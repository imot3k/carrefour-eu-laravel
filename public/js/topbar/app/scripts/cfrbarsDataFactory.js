define([], function() {

  'use strict';

  var cfrbarsDataFactory = function() {

    /**
     * @class cfrbarsData
     */
    var cfrbarsData = {

      init: function() {
        this.siteData = {};
        this.user = {};
      }

    };

    return cfrbarsData;

  };

  cfrbarsDataFactory.id = 'cfrbarsDataFactory';

  return cfrbarsDataFactory;

});
