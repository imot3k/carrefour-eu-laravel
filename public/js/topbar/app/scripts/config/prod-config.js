require.config({
	baseUrl: 'scripts',

	paths: {
		'angular': '../lib/bower/angular/angular',

		'pascalprecht.translate': '../lib/bower/angular-translate/angular-translate',

		'ngSanitize': '../lib/bower/angular-sanitize/angular-sanitize',

		'ngCookies': '../lib/bower/angular-cookies/angular-cookies',

		'clickOut': '../lib/bower/angular-clickout/angular-clickout',

		'underscore': '../lib/bower/lodash/lodash',

		'log': 'logging/console-logger',

		'text': '../lib/bower/requirejs-text/text',

		'components': '../components',

		'cfrbars': '../components/cfrbars/cfrbars',

		'cfrtopbar': '../components/cfrtopbar/main/cfrtopbar',

		'TopbarCtrl': '../components/cfrtopbar/main/TopbarCtrl',

		'cfrtopbarMenu': '../components/cfrtopbar/menu/cfrtopbar-menu',

		'cfrtopbarMenuitem': '../components/cfrtopbar/menuitem/cfrtopbar-menuitem',

		'cfrtopbarMenuitemCtrl': '../components/cfrtopbar/menuitem/cfrtopbar-MenuitemCtrl',

		'cfrtopbarLink': '../components/cfrtopbar/link/cfrtopbar-link',

		'cfrtopbarLogin': '../components/cfrtopbar/login/cfrtopbar-login',

		'TopbarLoginCtrl': '../components/cfrtopbar/login/TopbarLoginCtrl',

		'cfrtopbarLanguageswitcher': '../components/cfrtopbar/languageswitcher/cfrtopbar-languageswitcher',

		'languageswitcherCtrl': '../components/cfrtopbar/languageswitcher/languageswitcherCtrl',

		'cfrbottombar': '../components/cfrbottombar/main/cfrbottombar',

		'cfrbottombarCtrl': '../components/cfrbottombar/main/cfrbottombarCtrl',

		'cfrbottombarBlock': '../components/cfrbottombar/block/cfrbottombar-block',

		'cfrcookiebar': '../components/cfrcookiebar/main/cfrcookiebar',

		'CookieCtrl': '../components/cfrcookiebar/main/CookieCtrl',

		'cfrfooter': '../components/cfrfooter/cfrfooter',

		'FooterCtrl': '../components/cfrfooter/FooterCtrl',

		'translations_nl': '../translations/nl',

		'translations_fr': '../translations/fr'
	},

	shim: {
		'angular': {
			exports: 'angular'
		},
		'pascalprecht.translate': ['angular'],
		'ngSanitize': ['angular'],
		'ngCookies': ['angular'],
		'clickOut': ['angular']
	}
});
