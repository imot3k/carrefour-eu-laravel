define(['cfrbarsDataFactory'], function() {

  'use strict';

  var cfrbarsService = function($http, $q, $cookies, cfrbarsData, topbarConfig, cfrbarsLanguageUrlFilter) {

    return {

      /**
       * Load the data for the cfr bars.
       */
      loadSiteData: function() {

        var deferredData = $q.defer();

        $http.get(cfrbarsLanguageUrlFilter(topbarConfig.siteDataLocation), {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
          .success(function(data) {
            cfrbarsData.siteData = data;
            deferredData.resolve();
          })
          .error(function() {
            deferredData.reject();
          });

        return deferredData.promise;
      },

      /**
       * Load the user data for the cfr bars.
       */
      loadCurrentUser: function() {

        var carrefourUserSessionId = $cookies.get('carrefour_user_session_id');
        var carrefourUserToken = $cookies.get('carrefour_user_token');
        var deferredUser = $q.defer();

        // No cookies = no user.
        if (!carrefourUserSessionId || !carrefourUserToken) {
          deferredUser.resolve();
        } else {

          $http.get(cfrbarsLanguageUrlFilter(topbarConfig.userDataLocation), {
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            params: {
              sessionId: carrefourUserSessionId,
              token: carrefourUserToken
            }
          })
          .success(function(data) {
            if (data.name) {
              cfrbarsData.user = data;
            }
            deferredUser.resolve();
          })
          .error(function() {
            deferredUser.reject();
          });

        }

        return deferredUser.promise;
      }

    };

  };

  cfrbarsService.$inject = [
    '$http',
    '$q',
    '$cookies',
    'cfrbarsDataFactory',
    'topbarConfig',
    'cfrbarsLanguageUrlFilter'
  ];
  cfrbarsService.id = 'cfrbarsService';

  return cfrbarsService;

});
