/*globals topbarConfig */
define(['require',
        'MainCtrl',
        'cfrbars',
        'cfrtopbar',
        'cfrtopbarMenu',
        'cfrtopbarMenuitem',
        'cfrtopbarLink',
        'cfrtopbarLogin',
        'cfrbottombar',
        'cfrbottombarBlock',
        'cfrcookiebar',
        'cfrfooter',
        'cfrbarsCalculatePadding',
        'cfrtopbarLanguageswitcher',
        'cfrbarsService',
        'cfrbarsDataFactory',
        'cfrbarsLanguageUrlFilter',
        'pascalprecht.translate',
        'translations_nl',
        'translations_fr',
        'ngCookies'
], function(require,
          MainCtrl,
          cfrbars,
          cfrtopbarMain,
          cfrtopbarMenu,
          cfrtopbarMenuitem,
          cfrtopbarLink,
          cfrtopbarLogin,
          cfrbottombarMain,
          cfrbottombarBlock,
          cfrcookiebarMain,
          cfrFooter,
          cfrbarsCalculatePadding,
          cfrtopbarLanguageswitcher,
          cfrbarsService,
          cfrbarsDataFactory,
          cfrbarsLanguageUrlFilter) {
  'use strict';

  var angular = require('angular');

  // Get current language.
  var languageAttribute = angular.element(document).find('html')[0].attributes.language;
  var language = 'nl';
  if (languageAttribute) {
    language = languageAttribute.nodeValue;
  }

  // Get language switch links.
  var languages = {};
  var linkElements = document.querySelectorAll('link');
  for (var i = 0; i < linkElements.length; i++) {
    var linkElement = linkElements[i];
    if (linkElement.rel && linkElement.rel === 'alternate' && linkElement.hreflang) {
      languages[linkElement.hreflang] = linkElement.href;
    }
  }
  if (!languages.nl && topbarConfig.baseUrl) {
    languages.nl = topbarConfig.baseUrl.replace('{language}', 'nl');
  }
  if (!languages.fr && topbarConfig.baseUrl) {
    languages.fr = topbarConfig.baseUrl.replace('{language}', 'fr');
  }

  // Calculate the start offset as other css can add required margin and padding.
  var $bodyElement = angular.element(document).find('body');
  var $adminbar = $bodyElement[0].querySelectorAll('#admin-menu');
  var startMargin = 0;
  var startPadding = 0;
  var computed;
  var adminHeight;
  if (window.getComputedStyles) {
    computed = window.getComputedStyles($bodyElement[0]);
    startMargin = parseInt(computed['margin-top'], 10);
    startPadding = parseInt(computed['padding-top'], 10);
  }

  if (isNaN(startMargin)) {
    startMargin = 0;
  }
  if (isNaN(startPadding)) {
    startPadding = 0;
  }
  if ($adminbar.length) {
    adminHeight = $adminbar[0].offsetHeight;
    // Admin menu support. (body class is somtimes added later, so add fixed margin.
    if (startMargin < adminHeight) {
      startMargin += adminHeight;
    }
  }

  var app = angular.module('cfrbars', ['ngCookies', 'pascalprecht.translate'])
    .constant('START_PADDING_TOP', startPadding)
    .constant('START_MARGIN_TOP', startMargin)
    .constant('CURRENT_LANGUAGE', language)
    .constant('BU_COLOR', '#ababab')
    .constant('COLOR2', '#9d9d9d')
    .constant('COLOR3', '#484848')
    .constant('COLOR4', '#ccc')
    .constant('COLOR_TEXT', '#fff')
    .constant('LANGUAGES', languages)
    .constant('topbarConfig', topbarConfig)
    .config(['$translateProvider', function($translateProvider) {
      $translateProvider.translations(language, window['translations_' + language]);
      $translateProvider.preferredLanguage(language);
      $translateProvider.useSanitizeValueStrategy('escape');
    }]);

  app.controller(MainCtrl.id, MainCtrl)
    .directive(cfrbars.id, cfrbars)
    .directive(cfrtopbarMain.id, cfrtopbarMain)
    .directive(cfrtopbarMenu.id, cfrtopbarMenu)
    .directive(cfrtopbarMenuitem.id, cfrtopbarMenuitem)
    .directive(cfrtopbarLink.id, cfrtopbarLink)
    .directive(cfrbottombarMain.id, cfrbottombarMain)
    .directive(cfrbottombarBlock.id, cfrbottombarBlock)
    .directive(cfrcookiebarMain.id, cfrcookiebarMain)
    .directive(cfrFooter.id, cfrFooter)
    .directive(cfrtopbarLogin.id, cfrtopbarLogin)
    .directive(cfrtopbarLanguageswitcher.id, cfrtopbarLanguageswitcher)
    .directive(cfrbarsCalculatePadding.id, cfrbarsCalculatePadding)
    .service(cfrbarsService.id, cfrbarsService)
    .service(cfrbarsDataFactory.id, cfrbarsDataFactory)
    .filter(cfrbarsLanguageUrlFilter.id, cfrbarsLanguageUrlFilter);

  return app;
});
