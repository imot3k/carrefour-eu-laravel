define(['angular'], function(angular) {

  'use strict';

  var directive = function(START_PADDING_TOP, cfrbarsData, $timeout) {
    return {
      restrict: 'A',
      termninal: true,
      transclude: false,
      link: function($scope) {

        $timeout(updatePadding);

        // Handler to adjust heights of the top bars and adds them up as totalHeight.
        $scope.$on('updatePadding', updatePadding);
        /**
         * Listener to check if we need to update the padding.
         */
        function updatePadding() {

          var totalHeight = START_PADDING_TOP;

          // Calculate height of cookiebar.
          if (cfrbarsData.siteData.cookie.shown) {
            var cookiebar = angular.element(document)[0].querySelectorAll('.cfrcookiebar');
            if (cookiebar.length) {
              totalHeight += cookiebar[0].offsetHeight;
            }
          }
          // Calculate height of topbar menus.
          var topbarMenus = angular.element(document)[0].querySelectorAll('.cfrtopbar');
          if (topbarMenus.length) {
            totalHeight += topbarMenus[0].offsetHeight;
          }
          var body = angular.element(document).find('body');
          var style = body[0].getAttribute('style');
          if (style) {
            style += ';padding-top: ' + totalHeight + 'px;';
          } else {
            style = 'padding-top: ' + totalHeight + 'px;';
          }
          // Calculate height and make sure the general padding gets increased.
          body[0].setAttribute('style', style);

        }

      }
    };
  };

  directive.$inject = ['START_PADDING_TOP', 'cfrbarsDataFactory', '$timeout'];
  directive.id = 'cfrbarsCalculatePadding';

  return directive;
});
