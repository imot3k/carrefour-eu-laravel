define(['CookieCtrl', 'text!components/cfrcookiebar/main/template-cfrcookiebar.html'], function(CookieCtrl, template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        data: '='
      },
      template: template,
      controller: CookieCtrl
    };
  };

  directive.id = 'cfrcookiebar';

  return directive;
});
