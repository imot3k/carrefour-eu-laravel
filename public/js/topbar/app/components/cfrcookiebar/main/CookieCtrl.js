define(['cfrbarsDataFactory'], function(cfrbarsDataFactory) {

  'use strict';

  var CookieCtrl = function($scope,
                            $sce,
                            $cookies,
                            $translate,
                            cfrbarsData,
                            START_MARGIN_TOP,
                            START_PADDING_TOP,
                            BU_COLOR,
                            COLOR2,
                            COLOR3,
                            COLOR4,
                            COLOR_TEXT) {

    // Try to fetch cookie.
    var acceptance = parseInt($cookies.get('cfrcookiebar'), 10) || -1;

    $scope.offsetTop = START_MARGIN_TOP + START_PADDING_TOP;

    // Link is already HTML, avoid sanitizing.
    $scope.link = $sce.trustAsHtml(cfrbarsData.siteData.cookie.link);
    $scope.label = cfrbarsData.siteData.cookie.label || $translate.instant('DEFAULT_COOKIE_ACCEPT_LABEL');
    $scope.showCookie = cfrbarsData.siteData.cookie.updated > acceptance;
    $scope.message = cfrbarsData.siteData.cookie.message;
    $scope.buColor = cfrbarsData.siteData.buColor || BU_COLOR;
    $scope.color2 = cfrbarsData.siteData.color2 || COLOR2;
    $scope.color3 = cfrbarsData.siteData.color3 || COLOR3;
    $scope.color4 = cfrbarsData.siteData.color4 || COLOR4;
    $scope.colorText = COLOR_TEXT;

    cfrbarsData.siteData.cookie.shown = $scope.showCookie;

    $scope.$parent.$emit('updatePadding', true);

    $scope.clickButton = function() {
      var current = Math.round((new Date().getTime()) / 1000);
      // Update cookie.
      $cookies.put('cfrcookiebar', current);
      $scope.showCookie = false;
      cfrbarsData.siteData.cookie.shown = $scope.showCookie;
      $scope.$parent.$emit('updatePadding', true);
    };

    $scope.getClass = function() {
      return !$scope.showCookie ? 'cf-hidden' : '';
    }

  };

  CookieCtrl.$inject = [
    '$scope',
    '$sce',
    '$cookies',
    '$translate',
    'cfrbarsDataFactory',
    'START_MARGIN_TOP',
    'START_PADDING_TOP',
    'BU_COLOR',
    'COLOR2',
    'COLOR3',
    'COLOR4',
    'COLOR_TEXT'
  ];

  CookieCtrl.id = 'CookieCtrl';

  return CookieCtrl;
});
