define(['MainCtrl', 'text!components/cfrbars/template-cfrbars.html'], function(MainCtrl, template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      template: template,
      controller: MainCtrl
    };
  };

  directive.id = 'cfrbars';

  return directive;
});
