define(['cfrbottombarCtrl', 'text!components/cfrbottombar/main/template-cfrbottombar.html'], function(cfrbottombarCtrl, template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      template: template,
      controller: cfrbottombarCtrl,
      link: function($scope) {

        // We need timeout to get correct heights.
        //$scope.$evalAsync(equalHeight);
        setTimeout(equalHeight, 1);

        /**
         * Make sure all footer blocks have equal height.
         */
        function equalHeight() {

          var $group = document.querySelector('.cfrbottombar--group');
          if ($group) {
            var $menuwraps = $group.querySelectorAll('.cfrbottombar--menuwrap');
            var maxHeight = 0;
            angular.forEach($menuwraps, function(value) {
              if (value.offsetHeight > maxHeight) {
                maxHeight = value.offsetHeight;
              }
            });
            $scope.maxHeight = maxHeight + 'px';
            $scope.$apply();
          }

        }

      }
    };
  };

  directive.id = 'cfrbottombar';

  return directive;
});
