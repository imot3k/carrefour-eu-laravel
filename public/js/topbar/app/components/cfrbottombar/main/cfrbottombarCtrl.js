define(['cfrbarsDataFactory'], function(cfrbarsDataFactory) {

  'use strict';

  var cfrbottombarCtrl = function($scope, $sce, cfrbarsData) {

    $scope.footerblocks = {};
    $scope.extraStyles = '';
    var leftColor = 'bfbfbf';
    var rightColor = '717171';

    // Footerblocks HTML content massage.
    if (cfrbarsData.siteData.footerblocks) {
      var footerBlocks = cfrbarsData.siteData.footerblocks.blocks,
        count = 0;
      for (var i in footerBlocks) {

        if (footerBlocks.hasOwnProperty(i)) {

          if (footerBlocks[i].titleColor) {
            footerBlocks[i].titleStyle = '{"color": "#' + footerBlocks[i].titleColor + '"}';
          }

          if (footerBlocks[i].backgroundColor) {

            if (i == 'left1') {
              leftColor = footerBlocks[i].backgroundColor;
            }
            else if (i == 'right1') {
              rightColor = footerBlocks[i].backgroundColor;
            }

            footerBlocks[i].backgroundStyle = '{"background-color": "#' + footerBlocks[i].backgroundColor + '"}';
          }

          // These 3 need to NOT be sanitized.
          if (footerBlocks[i].content) {
            footerBlocks[i].content = $sce.trustAsHtml(footerBlocks[i].content);
          }
          if (footerBlocks[i].css) {
            footerBlocks[i].css = $sce.trustAsHtml(footerBlocks[i].css);
          }
          if (footerBlocks[i].js) {
            footerBlocks[i].js = '<script>' + footerBlocks[i].js + '</script>';
            footerBlocks[i].js = $sce.trustAsHtml(footerBlocks[i].js);
          }

          // Keep a counter to wrap per 3.
          count++;
          if (count <= 3) {
            $scope.footerblocks.first = $scope.footerblocks.first || {};
            $scope.footerblocks.first[i] = footerBlocks[i];
          } else {
            $scope.footerblocks.second = $scope.footerblocks.second || {};
            $scope.footerblocks.second[i] = footerBlocks[i];
          }
        }
      }

      $scope.extraStyles += '.cfrbottombar--block a.cfr-button { background-color: ' + cfrbarsData.siteData.footerblocks.buttonBackgroundColor + ';}';
      $scope.extraStyles += '.cfrbottombar--block a.cfr-button:hover { background-color: ' + cfrbarsData.siteData.footerblocks.buttonHoverBackgroundColor + ';}';

    }

    $scope.extraStyles += '.cfrbottombar--group-first { background: linear-gradient(to right, ' + leftColor + ' 49%, #' + rightColor + ' 50%);}';


  };

  cfrbottombarCtrl.$inject = ['$scope', '$sce', 'cfrbarsDataFactory'];

  cfrbottombarCtrl.id = 'cfrbottombarCtrl';

  return cfrbottombarCtrl;
});
