define(['text!components/cfrbottombar/block/template-cfrbottombar-block.html'], function(template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        block: '='
      },
      template: template
    };
  };

  directive.id = 'cfrbottombarblock';

  return directive;
});
