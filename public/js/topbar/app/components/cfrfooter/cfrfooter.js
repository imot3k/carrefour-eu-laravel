define(['FooterCtrl', 'text!components/cfrfooter/template-cfrfooter.html'], function(FooterCtrl, template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      template: template,
      controller: FooterCtrl,
      scope: {
        data: '='
      }
    };
  };

  directive.id = 'cfrfooter';

  return directive;
});
