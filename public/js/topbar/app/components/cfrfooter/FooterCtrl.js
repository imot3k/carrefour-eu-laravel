define(['cfrbarsDataFactory'], function(cfrbarsDataFactory) {

  'use strict';

  var FooterCtrl = function($scope,
                            cfrbarsData,
                            BU_COLOR,
                            COLOR2,
                            COLOR3,
                            COLOR4,
                            COLOR_TEXT) {

    $scope.buColor = cfrbarsData.siteData.buColor || BU_COLOR;
    $scope.color2 = cfrbarsData.siteData.color2 || COLOR2;
    $scope.color3 = cfrbarsData.siteData.color3 || COLOR3;
    $scope.color4 = cfrbarsData.siteData.color4 || COLOR4;
    $scope.colorText = COLOR_TEXT;

  };

  FooterCtrl.$inject = [
    '$scope',
    'cfrbarsDataFactory',
    'BU_COLOR',
    'COLOR2',
    'COLOR3',
    'COLOR4',
    'COLOR_TEXT'
  ];

  FooterCtrl.id = 'FooterCtrl';

  return FooterCtrl;
});
