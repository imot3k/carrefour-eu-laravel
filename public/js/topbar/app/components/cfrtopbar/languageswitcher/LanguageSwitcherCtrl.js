define(function() {

  'use strict';

  var LanguageSwitcherCtrl = function($scope, LANGUAGES, CURRENT_LANGUAGE) {

    $scope.menuShown = false;
    $scope.languages = LANGUAGES;
    $scope.current_language = CURRENT_LANGUAGE;

    /**
     * Show / hide the login dropdown.
     */
    $scope.toggleMenu = function(e) {
      e.preventDefault();
      $scope.menuShown = !$scope.menuShown;
    };

    /**
     * Hide the login dropdown.
     */
    $scope.hideMenu = function() {
      $scope.menuShown = false;
    }

  };

  LanguageSwitcherCtrl.$inject = ['$scope', 'LANGUAGES', 'CURRENT_LANGUAGE'];

  LanguageSwitcherCtrl.id = 'LanguageSwitcherCtrl';

  return LanguageSwitcherCtrl;
});
