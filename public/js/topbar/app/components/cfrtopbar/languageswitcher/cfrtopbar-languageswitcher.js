define([
  'text!components/cfrtopbar/languageswitcher/template-cfrtopbar-languageswitcher.html',
  'languageswitcherCtrl',
  'clickOut'
], function(template, languageswitcherCtrl) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        member: '='
      },
      template: template,
      controller: languageswitcherCtrl
    };
  };

  directive.id = 'cfrtopbarLanguageswitcher';

  return directive;
});
