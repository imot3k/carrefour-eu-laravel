define(['TopbarLoginCtrl', 'text!components/cfrtopbar/login/template-login.html'], function(TopbarLoginCtrl, template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        data: '='
      },
      controller: TopbarLoginCtrl,
      template: template
    };
  };

  directive.id = 'cfrtopbarlogin';

  return directive;
});
