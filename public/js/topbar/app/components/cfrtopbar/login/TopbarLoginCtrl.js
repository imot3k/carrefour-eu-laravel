define(function() {

  'use strict';

  var TopbarLoginCtrl = function($scope, topbarConfig) {

    $scope.menuShown = false;
    $scope.loginUrl = topbarConfig.loginUrl;
    $scope.registrationUrl = topbarConfig.registrationUrl;

    /**
     * Show / hide the login dropdown.
     */
    $scope.toggleMenu = function(e) {
      e.preventDefault();
      $scope.menuShown = !$scope.menuShown;
    };

    /**
     * Hide the login dropdown.
     */
    $scope.hideMenu = function() {
      $scope.menuShown = false;
    }

  };

  TopbarLoginCtrl.$inject = ['$scope', 'topbarConfig'];

  TopbarLoginCtrl.id = 'TopbarLoginCtrl';

  return TopbarLoginCtrl;
});
