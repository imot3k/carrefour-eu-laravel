define(function() {

  'use strict';

  var TopbarCtrl = function($scope,
                            $element,
                            $translate,
                            cfrbarsData,
                            CURRENT_LANGUAGE,
                            BU_COLOR,
                            COLOR2,
                            COLOR3,
                            COLOR4,
                            COLOR_TEXT) {
    
    $scope.user = cfrbarsData.user;
    $scope.menus = cfrbarsData.siteData.menus || [];
    $scope.current_language = CURRENT_LANGUAGE;
    $scope.buColor = cfrbarsData.siteData.buColor || BU_COLOR;
    $scope.color2 = cfrbarsData.siteData.color2 || COLOR2;
    $scope.color3 = cfrbarsData.siteData.color3 || COLOR3;
    $scope.color4 = cfrbarsData.siteData.color4 || COLOR4;
    $scope.colorText = COLOR_TEXT;

    // If we have user data, use it as label for user menu.
    // If not, remove the user menu.
    if (cfrbarsData.user && $scope.menus['user-menu']) {
      $scope.menus['user-menu'].label = $translate.instant('WELCOME_MESSAGE') + ' ' + cfrbarsData.user.name;
      if (parseInt(cfrbarsData.user.points) == 1) {
        $scope.menus['user-menu'].label_prefix = cfrbarsData.user.points +  ' ' + $translate.instant('POINT');
      }
      else {
        var points = ("" + cfrbarsData.user.points).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, function($1) { return $1 + "." });
        $scope.menus['user-menu'].label_prefix =  points +  ' ' + $translate.instant('POINTS');
      }

      $scope.menus['user-menu'].logout = topbarConfig.logoutUrl;
    }
    else {
      delete $scope.menus['user-menu'];
    }

    /**
     * Toggle the active state of a menu.
     */
    $scope.toggleActive = function(e, menu) {
      if (menu.tree) {
        e.preventDefault();
        menu.active = !menu.active;
      }
    };

    /**
     * Disable a menu.
     */
    $scope.disableActive = function(menu) {
      menu.active = false;
    };

  };

  TopbarCtrl.$inject = [
    '$scope',
    '$element',
    '$translate',
    'cfrbarsDataFactory',
    'CURRENT_LANGUAGE',
    'BU_COLOR',
    'COLOR2',
    'COLOR3',
    'COLOR4',
    'COLOR_TEXT'
  ];

  TopbarCtrl.id = 'TopbarCtrl';

  return TopbarCtrl;
});
