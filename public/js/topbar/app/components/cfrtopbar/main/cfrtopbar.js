define(['TopbarCtrl', 'text!components/cfrtopbar/main/template-cfrtopbar.html'], function(TopbarCtrl, template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        data: '='
      },
      template: template,
      controller: TopbarCtrl
    };
  };

  directive.id = 'cfrtopbar';

  return directive;
});
