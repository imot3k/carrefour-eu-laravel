define(['text!components/cfrtopbar/menu/template-cfrtopbar-menu.html'], function(template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        menu: '=',
        logout: '='
      },
      template: template
    };
  };

  directive.id = 'cfrtopbarmenu';

  return directive;
});
