define(function() {

  'use strict';

  var MenuitemCtrl = function($scope) {
    $scope.toggleActive = function(e) {
      if ($scope.links.subtree) {
        e.preventDefault();
        $scope.links.active = !$scope.links.active;
      }
    };
    $scope.disableActive = function() {
      $scope.links.active = false;
    };
  };

  MenuitemCtrl.$inject = ['$scope'];

  MenuitemCtrl.id = 'MenuitemCtrl';

  return MenuitemCtrl;
});
