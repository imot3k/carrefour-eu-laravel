define(['cfrtopbarMenuitemCtrl', 'text!components/cfrtopbar/menuitem/template-cfrtopbar-menuitem.html', 'clickOut'], function(cfrtopbarMenuitemCtrl, template) {

  'use strict';

  var directive = function($compile) {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        links: '='
      },
      template: template,
      link: function($scope, $element) {
        if ($scope.links.subtree) {
            $element.append('<cfrtopbarmenu menu="links.subtree"></cfrtopbarmenu>');
            $compile($element.find('cfrtopbarmenu'))($scope);
        }
      },
      controller: cfrtopbarMenuitemCtrl
    };
  };

  directive.$inject = ['$compile'];

  directive.id = 'cfrtopbarmenuitem';

  return directive;
});
