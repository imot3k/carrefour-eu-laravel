define(['text!components/cfrtopbar/link/template-cfrtopbar-link.html', 'clickOut'], function(template) {

  'use strict';

  var directive = function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        member: '='
      },
      template: template,
      require: '^cfrtopbarmenuitem'
    };
  };

  directive.id = 'cfrtopbarlink';

  return directive;
});
