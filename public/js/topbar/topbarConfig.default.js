var topbarConfig = {
  'siteDataLocation': 'http://localhost:9000/dummydata/site-data.json',
  'userDataLocation': 'http://localhost:9000/dummydata/user-data.json',
  'logoutUrl' : 'http://bonuscard.dev.be.carrefour.eu/{language}/user/logout',
  'loginUrl' : 'http://bonuscard.dev.be.carrefour.eu/{language}/connect-sso/login',
  'registrationUrl' : 'http://bonuscard.dev.be.carrefour.eu/{language}/connect-sso/register'
};