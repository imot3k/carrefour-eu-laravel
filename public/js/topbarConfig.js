var topbarConfig = {
  'siteDataLocation': 'https://content.be.carrefour.eu/{language}/topbar/1?_format=json',
  'userDataLocation': 'https://content.be.carrefour.eu/{language}/user-info?_format=json',
  'logoutUrl' : 'https://bonuscard.be.carrefour.eu/{language}/user/logout',
  'loginUrl' : 'https://bonuscard.be.carrefour.eu/{language}/connect-sso/login',
  'registrationUrl' : 'https://bonuscard.be.carrefour.eu/{language}/connect-sso/register',
  'languageSwitchUrl' : 'https://bonuscard.be.carrefour.eu/{language}',
  'languages' : {
    'nl' : 'https://bonuscard.be.carrefour.eu/nl',
    'fr' : 'https://bonuscard.be.carrefour.eu/fr'
  }
};
