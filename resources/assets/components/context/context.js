var init = function() {
    helper.determineContexts();
};

var helper = {
	/**
	 * Validate the chapter context.
	 *
	 * @param {string} context
	 * @param {array} items
	 * @returns {boolean}
	 */
	validateContext: function(context, items) {
		var valid = false;

		context = JSON.parse(context);

		for (var i in items) {
			if (items.hasOwnProperty(i) && items[i].title == context.title) {
				valid = true;
				break;
			}
		}

		return valid;
	},
	/**
	 * Set the current context.
	 *
	 * @param {object} node
	 * @param {string} name
	 * @param {string} field
	 */
	setContext: function(node, name, field) {
		var context = localStorage.getItem(name);
		var contextKnown = context !== null;

		if (node[field] && typeof node[field] !== "undefined") {
			var items = node[field];
			var keys = Object.keys(items);

			// Context is already known. Validate if the node contains this context.
			if (contextKnown && !helper.validateContext(context, items)) {
				// If the context was invalid, remove it.
				localStorage.removeItem(name);
			}

			if (keys.length === 1) {
				var key = keys[0];
				localStorage.setItem(name, JSON.stringify(items[key]));
			}
		}
	},
	/**
	 * Set the campaign context when visiting a campaign page.
	 *
	 * @param {object} node
	 */
	setCampaignContext: function (node) {
		var context = {
			"nid": node.nid,
			"title": node.title,
			"path": node.path,
            "chapters": {}
		};
		jQuery.each(node.field_chapters, function(index, item) {
			context.chapters[item.nid] = item;
		});
		localStorage.setItem("campaign_context", JSON.stringify(context));
	},
	/**
	 * Set the campaign context when visiting a campaign page.
	 *
	 * @param {object} node
	 */
	setChapterContext: function (node) {
		var context = {
			"nid": node.nid,
			"title": node.title,
			"path": node.path
		};

		localStorage.setItem("chapter_context", JSON.stringify(context));
	},
	/**
	 * Determine the chapter and campaign contexts for the current node.
	 */
	determineContexts: function() {
		if (window.currentNode && typeof window.currentNode !== "undefined") {
			var node = window.currentNode;

			switch (node.type) {
				case 'article':
				case 'recipe':
					helper.setContext(node, "campaign_context", "campaigns");
					helper.setContext(node, "chapter_context", "field_chapters");
					break;
				case 'campaign':
					helper.setContext(node, "chapter_context", "field_chapters");
					localStorage.removeItem("campaign_context");
					break;
		}
		}
	},
	/**
	 * Sets the context for the current node
	 */
	setContexts: function() {
		if (window.currentNode && typeof window.currentNode !== "undefined") {
			var node = window.currentNode;
			switch (node.type) {
				case 'campaign':
					helper.setCampaignContext(node);
					break;
				case 'chapter':
					helper.setChapterContext(node);
					localStorage.removeItem("campaign_context");
					break;
			}
		}
	}
};

exports.init = init;
exports.helper = helper;
