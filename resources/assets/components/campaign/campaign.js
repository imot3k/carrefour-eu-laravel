var init = function() {
	var $campaign = $('.campaignItem.dView');

	if ($campaign.length) {
		var campaignId = window.currentNode.nid;
		var language = $('html').attr('lang');

		// fetch related articles/campaigns
		$.get('/' + language + '/ajax/api-data', {
			resource: 'related',
			id: $('body').attr('data-id'),
			template: 'helpers/related',
			viewData: {campaignId:campaignId},
			query: {
				campaign: campaignId,
				language: language
			}
		}, function(response) {
			$campaign.find('> .wrap > article > .wrap2 > aside').prepend(response);
		});
	}

};

exports.init = init;
