var prevValue = '';

var init = function() {

   $('.extraFilters').cfExpandCollapse({
       handle: '> header',
       class: 'jOpen',
       clickOutsideMonitoring: false,
       onChange: function($this, state) {
           $('.recipeFilters').toggleClass('jFiltered');
       }
    });


    $('.recipeFilters .extraFilters > .main .inpCheckbox').cfExpandCollapse({
        handle: '> .label',
        class: 'jOpen',
        clickOutsideMonitoring: false
    });

    $('.recipeFilters form').submit(function(e) {
    	e.preventDefault();
	});

    /* fixed list for testing */
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };


	facets();

};

var filterResults = function() {
	var filteredFacets = $('.inpCheckbox input:checked');
	var filteredFacetIds = {};
	filteredFacets.each(function(index, item) {
		var parent = $(this).closest('.inpCheckbox');
		if (typeof filteredFacetIds[parent.attr('data-field')] == 'undefined') filteredFacetIds[parent.attr('data-field')] = [];
		filteredFacetIds[parent.attr('data-field')].push(parseInt($(item).val()));
	});
	for (var field in filteredFacetIds) {
		filteredFacetIds[field] = filteredFacetIds[field].join(',');
	}
	filteredFacetIds.search = $('#filterRecipesSearch').val();

	$('.recipeFilters').addClass('jSearching');

	var loadMore = $('.teaserGrid .teaserTab:first').data('loadMore');
	loadMore.registerFilters(filteredFacetIds);
	loadMore.loadItems(0, true, function(data) {
		$('.recipeFilters').removeClass('jSearching');
		var filters = data.filters;
		$.each(filters, function(vindex, voc) {
			$.each(voc.terms, function(tindex, term) {
				var termItem = $('[name="filterOption' + vindex+'"][value="' + term.tid + '"]').parent();
				if (term.count == 0) {termItem.addClass('jEmpty'); termItem.find('input').attr('disabled',true);}
				else {termItem.removeClass('jEmpty'); termItem.find('input').removeAttr('disabled');}
			});
		});
	});
};

var facets = function() {
	var searchTimeout;


	$('.inpCheckbox input').change(function(e) {
		if ($(this).is(':checked')) {
			// add filter to summary
			var summary = $('<li><a href="#">' + $(this).siblings('label').text() + '</a></li>');
			summary.data('filter', $(this));
			$('.nav.filteredOverview ul').append(summary);
			$(this).data('summary', summary);
		} else {
			$(this).data('summary').remove();
			// remove filter from summary
		}
		filterResults();
	});

	$('.nav.filteredOverview').on('click', 'li a', function(e) {
		e.preventDefault();
		$(this).parent().data('filter').prop('checked', false);
		$(this).parent().remove();
		filterResults();
	});


	$('#filterRecipesSearch').on('keyup change blur', function(e) {
		var currentValue = $(this).val();
		if (currentValue != prevValue) {
			if (searchTimeout) {
				clearTimeout(searchTimeout);
			}

			searchTimeout = setTimeout(function () {
				filterResults();
			}, 500);
		} prevValue = currentValue;
	});
}

exports.init = init;
