var openClass = 'jOpen';
var isMobile = false;
var activeFilters = {
    resource: 'masonry',
    template: 'helpers/teaserGridContent',
    query: {
        limit: 8,
        offset: 0,
        language: 'nl'
	}
};
var isLoading = false;
var $container,$content,$footer;

var init = function() {
    resize();

    $container = $('.teaserGrid');
    $content = $container.find('> .main');
    $footer = $container.find('> footer');

	$container.find('.teaserTab:not(:first)').addClass('jHide');

    $container.find('.teaserTab').each(function(index, item) {
    	if ($(item).attr('data-resource') != '') $(item).data('loadMore', new loadMore($(item), {resource: $(item).attr('data-resource'), id: $(item).attr('data-id'), limit: parseInt($(item).attr('data-limit'))}));
    	else {
    		$(item).find('.lnkLoadmore').hide();
		}
	});


    $('.teaserGrid > header .nav.gridFilter > ul > li').click(function(e) {
        var container = $(this).closest('li');
        var gridContainer = container.closest('.teaserGrid');
        var flyoutContainer = gridContainer.find('> header #subFilter');
        var link = $(this).find('>a');

        if (container.hasClass('ignoreTab')) {
			if (e.target.tagName == 'LI') {
				e.preventDefault();
				window.location = link.attr('href');
			}
		} else {
			e.preventDefault();
			// remove open class from siblings
			container.siblings().removeClass(openClass).css('marginBottom', 0);
			// add open class to container
			container.addClass(openClass);
			// empty flyout container
			flyoutContainer.empty();
			if (container.hasClass('hasSub')) {
				var subLevel = container.find('> ul').clone(true);
				flyoutContainer.append(subLevel);
				drawFlyout(container, flyoutContainer);
			} else {
				// do something else
			}

			gridContainer.find('.teaserTab').addClass('jHide');
			gridContainer.find(link.attr('href')).removeClass('jHide');
		}

    });
};


/**
 * Draws the flyout, determines, on mobile adds margin-bottom and positioning to flyout
 * @param container
 * @param flyoutContainer
 */
var drawFlyout = function(container, flyoutContainer) {
    if (isMobile) {
        container.css('marginBottom', flyoutContainer.outerHeight());
        var offset = container.position();
        flyoutContainer.css('top', offset.top + container.outerHeight());
    } else {
        container.css('marginBottom', 0);
        flyoutContainer.css('top', '');
    }
};

/**
 * Window resize function
 */
var resize = function() {
    isMobile = window.matchMedia("(max-width: 549px)").matches;
    var openItems = $('.teaserGrid > header .nav.gridFilter > ul > li.' + openClass);
    if (openItems.length) {
        openItems.each(function(index, item) {
            var container = $(item);
            var gridContainer = container.closest('.teaserGrid');
            var flyoutContainer = gridContainer.find('> header #subFilter');
            drawFlyout(container, flyoutContainer);
        });
    }
};

$(window).resize(function() {
    resize();
});

var loadMore = function(element, options) {
	var _ = this;

	_.$element = element;
	_.$footer = _.$element.find('.lnkLoadmore');
	_.$footerLink = _.$footer.find('a');
	_.options = options;
	_.isLoading = false;
	_.filters = {};

	_.options.offset = _.$element.find('.ctItem').length - _.options.limit;
	_.options.language = $('html').attr('lang');
	_.options.index = _.$element.attr('data-index');
	_.options.template = _.$element.attr('data-template');

	if (_.$element.find('.ctItem.jHide').length == 0 && _.$element.find('.ctItem').length < _.options.limit) _.$footerLink.hide();

	// load more button click
	_.$footerLink.click(function(e) {
		e.preventDefault();

		// first try to show the hidden items
		if (_.$element.find('.ctItem.jHide').length) {
			_.$element.find('.ctItem.jHide').removeClass('jHide');
		} else {
			// if no hidden items present, perform ajax call
			if (!isLoading) {
				var offset = _.options.offset + _.options.limit;
				_.loadItems(offset);
			}
		}
	});
};

loadMore.prototype.loading = function(state) {
	var _ = this;
	_.isLoading = state;
	if (state) _.$element.addClass('jLoading');
	else _.$element.removeClass('jLoading');
};

loadMore.prototype.loadItems = function(offset, clear, returnDataCallback) {
	var _ = this;
	_.options.offset = offset;

	// merge query with active filters
	var query = $.extend( {}, _.filters, {
		limit: _.options.limit + 1,
		offset: _.options.offset,
		language: _.options.language
	});

	var call = {
		resource: _.options.resource,
		id: _.options.id,
		template: (_.options.template ? _.options.template : 'helpers/teaserGridContent'),
		query: query,
		viewData: {
			printLimit: _.options.limit,
			offset: _.options.offset
		}
	};

	if (typeof returnDataCallback != 'undefined' && returnDataCallback) call.returnData = true;

	// show loading animation
	_.loading(true);

	if (typeof teaserOffset[_.options.index] != 'undefined') {
		call.viewData.teaserOffset = (offset == 0 ? 0 : teaserOffset[_.options.index]);
		call.viewData.index = _.options.index;
	}

	// fetch data
	$.get('/' + $('html').attr('lang') + '/ajax/api-data', call, function (response) {
		// check if container should be cleared first
		if (typeof clear != 'undefined' && clear) _.clear();
		if (typeof returnDataCallback != 'undefined' && returnDataCallback) {
			jsonData = jQuery.parseJSON(response);
			response = jsonData.html;
			returnDataCallback(jsonData.data);
		}
		// insert new items
		$(response).insertBefore(_.$footer);

		// determine whether the load more button should be shown
		if (!response || $(response).filter('.ctItem').length < _.options.limit || (typeof teaserHasMore[_.options.index] != 'undefined' && teaserHasMore[_.options.index] == false)) _.$footerLink.hide();
		else _.$footerLink.show();

		// hide loading animation
		_.loading(false);
	});

	// console.log(call);
};

loadMore.prototype.registerFilters = function(filters) {
	this.filters = filters;
};

loadMore.prototype.clear = function() {
	this.$element.find('.ctItem, .notification, script').remove();
};

exports.init = init;
