var videoPlaying = false;
var videoPlayers = [];

var init = function() {
    $('.sctBanners').each(function(index, item) {
        if ($(item).find('> .main > .bannerItem').length > 1) {
            $(item).find('> .main').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                speed: 300,
                fade: true,
                cssEase: 'linear'
            });
        }
        //$(item).find('> .main').append('<div class="lnkCloseVideo"><a href="#">Close</a></div>');
        $(item).find('.lnkCloseVideo a').click(function(e) {
            e.preventDefault();
            videoPlaying = false;
            $(item).find('.bannerItem').removeClass('jVideoPlaying');
            $(item).removeClass('jVideoPlaying');
            $(videoPlayers).each(function(index, item) {
                item.pauseVideo();
            });
        });
    });

    $('.sctBanners').on('click', '.bannerItem.hasVideo', function(e) {
        if (!videoPlaying) {
            var container = $(this);
            var parent = container.closest('.sctBanners');
            container.addClass('jVideoPlaying');
            parent.addClass('jVideoPlaying');
            videoPlaying = true;
            container.find('.video > *').data('yt').playVideo();
        }
    });

    if ($('.bannerItem.hasVideo').length) {
        window.YouTubeLoader.load(function(YT) {
            // find players and add them to the list
            $('.bannerItem.hasVideo .video > *').each(function (index, item) {
                var id = $(item).attr('id');
                var player = new YT.Player(item, {
                    videoId: $(item).attr('data-video'),
					events: {
						'onStateChange': function(event) {

						}
					}
                });
                $('#' + id).data('yt', player);
                videoPlayers.push(player);
            });
        });
    }
};

exports.init = init;
exports.videoPlaying = videoPlaying;
exports.videoPlayers = videoPlayers;

