var init = function() {
    $('.contentSlider .slider > .main').slick({
        arrows: true,
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        variableWidth: true
    });
};

exports.init = init;
