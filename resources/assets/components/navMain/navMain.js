var init = function() {
	$('.nav.main li.hasSub').cfExpandCollapse({
		handle: '> a',
		class: 'jShow',
		clickOutsideMonitoring: true
	});
};

exports.init = init;