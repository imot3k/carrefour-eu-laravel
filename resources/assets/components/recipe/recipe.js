
var videoPlayer;
var filters = {};

var init = function() {
    toggle();
	video();
	relatedRecipes();
	scrollOnFocus();
};

var scrollOnFocus = function() {
	var $filter = $(".frmFilterrecipes");
	var $input = $("#filterRecipesSearch", $filter);

	if ($filter.length) {
		$input.on('focus', function() {
			$('body').animate({
				scrollTop: $filter.offset().top - $(".cfr-topbar-container").height()
			}, 1000);
		});
	}
};

var toggle = function() {
	var openClass = 'jShow2p';
	var activeClass = 'jActive';

	// if cookies used, not always first-child
	$('.portionToggle li:last-child').addClass(activeClass);

	$('.portionToggle li').click(function(e) {
		e.preventDefault();

		if (!$(this).hasClass(activeClass)) {
			$('.portionToggle li.jActive').removeClass(activeClass);
			$(this).addClass(activeClass);

			$('#ingredients').toggleClass(openClass);
		}
		return false;
	});
};

var video = function() {
	// recipe video
	var recipeVideo = $('#recipeVideo');

	if (recipeVideo.length) {
		window.YouTubeLoader.load(function(YT) {
			// find players and add them to the list
			var id = recipeVideo.attr('id');
			var videoId = recipeVideo.attr('data-video');
			match = videoId.match(/youtu\.be\/([\s\S]+)|\/watch\?v=([^&]+)/im);
			if (match != null) {
				if (typeof match[2] != 'undefined') videoId = match[2];
				else videoId = match[1];
			}
			var player = new YT.Player('recipeVideo', {
				videoId: videoId,
				events: {
					'onReady': function(event) {},
					'onStateChange': function(event) {}
				}
			});
			$('#' + id).data('yt', player);
			videoPlayer = player;
		});

		$('.recipeItem.dView > .wrap > .header figure .image').click(function(e) {
			$('.recipeItem.dView').addClass('jVideoPlaying');
			videoPlayer.playVideo();
		});
	}
};


var relatedRecipes = function() {
	var $article = $('.recipeItem.dView');

	if ($article.length) {
		// fetch related articles/campaigns
		$.get('/ajax/api-data', {
			resource: 'related',
			id: $('body').attr('data-id'),
			template: 'helpers/related',
			query: {
				language: $('html').attr('lang')
			}
		}, function (response) {
			//$article.find('> .wrap > article > .wrap2 > aside').prepend(response);
			$article.find('> .wrap > article > .wrap2 > aside > .shadowContent').after(response);
		});
	}
}


exports.init = init;
