var container;
var items;
var flyout;
var init = function() {
	// initialize context
	context();

	// check breadcrumb length
	if ($('.nav.breadcrumb li').length > 1) {
		container = $('.nav.breadcrumb');
		items  = $(container.find('> ul > li:not(.home,:last-child)').get().reverse());
		flyout = $('<div class="flyout"><div class="handle">...</div><ul></ul></div>');
		container.append(flyout);

		resize();
		$(window).resize(function(e) {
			resize();
		});

		flyout.cfExpandCollapse({
			handle: '> .handle',
			class: 'jShow',
			clickOutsideMonitoring: true
		});
	}
};

var resize = function() {
	flyout.find('ul').empty();
	items.hide();
	container.find('> ul > li:last-child').css('maxWidth', '');
	var maxWidth = container.width() - container.find('> ul > li:last-child').width() - container.find('> ul > li.home').width() - 31;
	var currentWidth = 0;

	if (maxWidth <= 0) {
		container.find('> ul > li:last-child').css('maxWidth', container.width() - container.find('> ul > li.home').width() - 31);
	}

	$(items).each(function(index, item) {
		if (currentWidth + $(item).width() >= maxWidth) {
			$(item).hide();
			var clone = $(item).clone().show();
			flyout.find('ul').prepend(clone);
		} else {
			$(item).show();
		}
		currentWidth += $(item).width();
	});

	flyout.find('ul').css('maxWidth', container.width() - 31);

	if (flyout.find('ul > li').length) {
		container.addClass('jHasFlyout');
	} else {
		container.removeClass('jHasFlyout');
	}
};

var context = function() {
	var campaignData = localStorage.getItem('campaign_context');
	var chapterData = localStorage.getItem('chapter_context');

	var lastItem = $('.nav.breadcrumb > ul > li:last');

	var chapter, chapterItem, siloKeys, siloItem;

	if ($('.campaignItem.dView, .recipeItem.dView, .articleItem.dView').length) {
		if (campaignData) {
			var campaign = jQuery.parseJSON(campaignData);
			var navItems = [];
			if (campaign.nid) {
				$('.nav.breadcrumb > ul > li:not(:first,:last)').remove();
				var chapterKeys = Object.keys(campaign.chapters);
				if (chapterKeys.length) {
					if (chapterData) {
						chapter = jQuery.parseJSON(chapterData);
						chapterItem = false;
						// determine chapter
						if (chapterKeys.length == 1) {
							chapterItem = campaign.chapters[chapterKeys[0]];
						} else if (campaign.chapters.hasOwnProperty(chapter.nid)) {
							chapterItem = campaign.chapters[chapter.nid];
						}
						if (chapterItem) {
							siloKeys = Object.keys(chapterItem.silos);
							 if (siloKeys.length == 1) {
								 siloItem = chapterItem.silos[siloKeys[0]];
								 $('<li data-ref="silo"><span>' + siloItem.title + '</span></li>').insertBefore(lastItem);
								 $('nav.main li[data-id="' + siloItem.nid + '"]').addClass('active');
							 }
							$('<li data-ref="chapter-1"><a href="' + chapterItem.path + '">' + chapterItem.title + '</a></li>').insertBefore(lastItem);
						}
					}
				}
				$('<li data-ref="campaign"><a href="' + campaign.path + '">' + campaign.title + '</a></li>').insertBefore(lastItem);
			}
		} else if (chapterData) {
			$('.nav.breadcrumb > ul > li:not(:first,:last)').remove();
			chapter = jQuery.parseJSON(chapterData);
			if (chapter.hasOwnProperty('silos')) {
				siloKeys = Object.keys(chapter.silos);
				if (siloKeys.length == 1) {
					siloItem = chapter.silos[siloKeys[0]];
					$('<li data-ref="silo-2"><span>' + siloItem.title + '</span></li>').insertBefore(lastItem);
					$('nav.main li[data-id="' + siloItem.nid + '"]').addClass('active');
				}
			}
			$('<li data-ref="chapter-2"><a href="' + chapter.path + '">' + chapter.title + '</a></li>').insertBefore(lastItem);
		}
	} else {
		if ($('nav.main > ul > li.active').length == 0) {
			$('nav.main > ul > li').each(function(index, item) {
				var id = $(item).attr('data-id');
				if (id && $('.nav.breadcrumb li[data-id="' + id + '"]').length) $(item).addClass('active');
			});
		}
	}
};


exports.init = init;
