var activeClass = 'jShowSearch';
var close = function() {
    $('body').removeClass(activeClass);
    $('html, .navigation .search').off('click.outsideClick');
};
var open = function() {
    $('body').addClass(activeClass);
    $('html').one('click.outsideClick', function() {
        // console.log('click outside');
        close();
    });

    $('.navigation .search').on('click.outsideClick', function(e) {
        e.stopPropagation();
    });
}

var init = function () {
    $('.nav.skip .search a').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
       if ($('body').hasClass(activeClass)) {
           close();
       }  else {
           open();
       }
    });
};
exports.init = init;
