var init = function() {
  $('.focusFolders .slider > .main').slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: true,
      dots: true,
      responsive: [
          {
              breakpoint: 1024,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          }
      ]
  });
};

exports.init = init;
