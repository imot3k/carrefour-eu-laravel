var videoPlayer;

var init = function() {
	relatedArticles();
    video();
	findMovies();
};

var findMovies = function() {
	$('.contentFree .figure-wrapper, article.embedded-entity').each(function(index, item) {
		if ($(item).find('.media-video').length) $(item).addClass('media-video');
		else if ($(item).find('.media-image').length) $(item).addClass('media-image');
	});
};

var relatedArticles = function() {
	var $article = $('.articleItem.dView');

	if ($article.length) {
		var campaignContext = localStorage.getItem('campaign_context');
		var campaignId;
		//console.log(campaignContext);
		if (campaignContext) {
			var campaign = jQuery.parseJSON(campaignContext);
			if (campaign.nid) campaignId = campaign.nid;
		}
		// console.log(campaignId);
		// fetch related articles/campaigns
		var query = {
			language: $('html').attr('lang')
		};
		if (campaignId) query.campaign = campaignId;

		$.get('/ajax/api-data', {
			resource: 'related',
			id: $('body').attr('data-id'),
			template: 'helpers/related',
			viewData: (campaignId ? {campaignId:campaignId} : {}),
			query: query
		}, function(response) {
			$article.find('> .wrap > article > .wrap2 > aside').prepend(response);
		});
	};
};


var video = function() {
    // recipe video
    var recipeVideo = $('#articleVideo');

    if (recipeVideo.length) {
        window.YouTubeLoader.load(function(YT) {
            // find players and add them to the list
            var id = recipeVideo.attr('id');
            var videoId = recipeVideo.attr('data-video');
            match = videoId.match(/youtu\.be\/([\s\S]+)|\/watch\?v=([^&]+)/im);
            if (match != null) {
                if (typeof match[2] != 'undefined') videoId = match[2];
                else videoId = match[1];
            }
            var player = new YT.Player('articleVideo', {
                videoId: videoId,
                events: {
                    'onReady': function(event) {},
                    'onStateChange': function(event) {}
                }
            });
            $('#' + id).data('yt', player);
            videoPlayer = player;
        });

        $('.articleItem.dView > .wrap > .header figure .image').click(function(e) {
            $('.articleItem.dView').addClass('jVideoPlaying');
            videoPlayer.playVideo();
        });
    }
};

exports.init = init;
exports.findMovies = findMovies;
