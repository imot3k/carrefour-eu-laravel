/*!
 Load More
 */

;(function ( $, window, document, undefined ) {
	// Create the defaults once
	var pluginName = 'cfLoadMore',
		defaults = {
			content: '',
			handle: '> a',
			url: '',
			loadingClass: 'jLoading'
		};

	// The actual plugin constructor
	function cfLoadMore( element, options ) {
		var _ = this;

		_.element = element;
		_.$element = $(element);
		_.options = $.extend( {}, defaults, options) ;

		_._defaults = defaults;
		_._name = pluginName;
		_.page = 1;

		_.$content = _.$element.find(_.options.content);
		_.$handle = _.$element.find(_.options.handle);

		_.init();
	}

	cfLoadMore.prototype.init = function () {
		var _ = this;

		_.$handle.click(function(e) {
			e.preventDefault();
			_.page += 1;
			_.$element.addClass(_.options.loadingClass);
			// console.log('loading more...');
			$.ajax({
				url: _.options.url,
				method: "GET",
				data: {resource: "silos", page: _.page},
				dataType: "json",
			}).done(function( html ) {
				//$this.content.append(html);
				alert('more loaded');
				_.$element.removeClass(_.options.loadingClass);
			});
		});

	};


	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName,
					new cfLoadMore( this, options ));
			}
		});
	}

})( jQuery, window, document );