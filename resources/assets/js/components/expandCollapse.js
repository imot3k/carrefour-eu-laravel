/*!
    Expand / Collapse
 */

;(function ( $, window, document, undefined ) {
	// Create the defaults once
	var pluginName = 'cfExpandCollapse',
		defaults = {
	        handle: '> a',
            class: 'jShow',
			clickOutsideMonitoring: true
        };

	// The actual plugin constructor
	function cfExpandCollapse( element, options ) {
	    var _ = this;

		_.element = element;
		_.$element = $(element);

		_.options = $.extend( {}, defaults, options) ;

		_._defaults = defaults;
		_._name = pluginName;

		_.$handle = _.$element.find(_.options.handle);
		//_.$classElement = (typeof _.options.classElement != 'undefined' ? $())

        //console.log(_.$element);
        //console.log(_.$handle);
		_.init();
	}

	cfExpandCollapse.prototype.init = function () {
	    var _ = this;
	   _.$handle.click(function(e) {
	        e.preventDefault();
			if (_.$element.hasClass(_.options.class)) {
				_.close();
			} else {
				$(this).trigger('click.outsideClick');
				_.open();
				//if (_.options.clickOutsideMonitoring) e.stopPropagation();
			}
        });
	};

	cfExpandCollapse.prototype.close = function() {
		var _ = this;
		_.$element.removeClass(_.options.class);
		if (typeof _.options.onChange != 'undefined') _.options.onChange(_, 0);
		if (_.options.clickOutsideMonitoring) {
			$('html').off('click.outsideClick');
			_.$element.off('click.outsideClick');
		}
    };

	cfExpandCollapse.prototype.open = function() {
	    var _ = this;

		_.$element.addClass(_.options.class);
		if (typeof _.options.onChange != 'undefined') _.options.onChange(_, 1);
		if (_.options.clickOutsideMonitoring) {
			$('html').one('click.outsideClick', function() {
				_.close();
			});
			_.$element.on('click.outsideClick', function(e) {
				e.stopPropagation();
			});
		}
    };

	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName,
					new cfExpandCollapse( this, options ));
			}
		});
	}

})( jQuery, window, document );
