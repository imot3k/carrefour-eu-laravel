window.$ = window.jQuery = require('jquery');
slick = require('slick-carousel');
expandCollapse = require('./components/expandCollapse');
fancybox = require('./components/jquery.fancybox');
//typeahead = require('typeahead.js');
window.YouTubeLoader = require('./components/youtubeLoader');

$(document).ready(function() {
	var context = require('../components/context/context');
	context.init();

	var banner = require('../components/banner/banner');
	banner.init();

	var contentSlider = require('../components/contentSlider/contentSlider');
	contentSlider.init();

	var contentGallery = require('../components/contentGallery/contentGallery');
	contentGallery.init();

	var teaserGrid = require('../components/teaserGrid/teaserGrid');
	teaserGrid.init();

	var recipe = require('../components/recipe/recipe');
	recipe.init();

	var recipeOverview = require('../components/pgRecipeOverview/pgRecipeOverview');
	recipeOverview.init();

	var article = require('../components/article/article');
	article.init();

	var campaign = require('../components/campaign/campaign');
	campaign.init();

	var folders = require('../components/pgHome/folders');
	folders.init();

	var navSkip = require('../components/navSkip/navSkip');
	navSkip.init();

	var navMain = require('../components/navMain/navMain');
	navMain.init();

	var search = require('../components/search/search');
	search.init();

    var navMain = require('../components/navMain/navMain');
    navMain.init();

    // temporarily disabled, will be needed again when non-food site launches
	// var navEnseigne = require('../components/navEnseigne/navEnseigne');
	// navEnseigne.init();

	var navBreadcrumb = require('../components/navBreadcrumb/navBreadcrumb');
	navBreadcrumb.init();


	$(".fancybox").fancybox({autoResize: true});


	context.helper.setContexts();

	// @TODO: remove if top offset is needed on navigation
    /*var $scope = angular.element($('html')[0]).scope();
     $scope.$on('updatePadding', function() {console.log('test'); });*/

    var hasJS = require('./components/hasJS');
    hasJS.init();
});