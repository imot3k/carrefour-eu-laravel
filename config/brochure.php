<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API endpoint
    |--------------------------------------------------------------------------
    */
//  'api_endpoint' => 'https://dev-soa.carrefour.eu',
//  'api_endpoint' => 'https://qa-soa.carrefour.eu',
    'api_endpoint' => 'https://soa.carrefour.eu',

    /*
    |--------------------------------------------------------------------------
    | API authentication
    |--------------------------------------------------------------------------
    */
//    'api_authentication' => ['autouser' => 'Transvweb_dev', 'autokey' => 'QpD%y4e3Kb'],
//    'api_authentication' => ['autouser' => 'Transvweb_qa', 'autokey' => 'oD3L")5Dg1'],
    'api_authentication' => ['autouser' => 'Transvweb', 'autokey' => 'kPt(9fdG$7m:4'],

    /*
    |--------------------------------------------------------------------------
    | API method
    |--------------------------------------------------------------------------
    */
    'api_method' => 'folder',

];


