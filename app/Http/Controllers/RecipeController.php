<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\App;
use App\Crf\Facades\Brochure;

class RecipeController extends Controller
{
	/**
	 * @var array
	 */
	private $viewData = [];

	/**
	 * @var string
	 */
	private $locale;

	/**
	 * @var ApiController
	 */
	private $api;

	public function __construct()
	{
		$this->locale = App::getLocale();
		$this->api = new ApiController();
		$this->viewData['locale'] = $this->locale;
	}

	public function index()
	{
		$this->setPathLanguages();

		// Get the silos for constructing the menu.
		$this->viewData['silos'] = $this->api->getData('silos', null, [
			'language' => $this->locale,
			'exclude' => 'content',
		]);

		$recipeData = $this->api->getData('recipes', null, [
			'language' => $this->locale,
			'limit' => 16
		]);
		$this->viewData['recipes'] = $recipeData->recipes;
		$this->viewData['filters'] = $recipeData->filters;

		return view('pages.recipe_overview', $this->viewData);
	}

    /**
     * Set language for paths.
     */
    private function setPathLanguages() {
        /* @var \Mcamara\LaravelLocalization\LaravelLocalization $localisation */
        $localisation = app('laravellocalization');
        foreach ($localisation->getSupportedLocales() as $lang => $value) {
            $url = $localisation->localizeURL(null, $lang);
            $this->viewData['paths'][$lang] = $url ? $url : '';
        }
    }
}
