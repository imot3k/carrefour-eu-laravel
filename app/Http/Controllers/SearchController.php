<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\App;
use App\Crf\Facades\Brochure;
use Illuminate\Http\Request as Request;

class SearchController extends Controller
{
	/**
	 * @var array
	 */
	private $viewData = [];

	/**
	 * @var string
	 */
	private $locale;

	/**
	 * @var ApiController
	 */
	private $api;

	public function __construct()
	{
		$this->locale = App::getLocale();
	}

	public function index(Request $request)
	{
		$this->validate($request,
			[
				'search' => 'required',
			]
		);
		$url = 'https://www.drive.be/'.$this->locale.'/recherche/'. str_replace('%', '$00', rawurlencode(utf8_decode($request->input('search')))) .'/0/1?store_ref=D0615';
		return redirect()->away($url);
	}
}
