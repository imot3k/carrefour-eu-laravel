<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\App;
use App\Crf\Facades\Brochure;

class HomeController extends Controller
{
    /**
     * @var array
     */
    private $viewData = [];

    /**
     * @var string
     */
    private $locale;

    /**
     * @var ApiController
     */
    private $api;

    public function __construct()
    {
        $this->locale = App::getLocale();
        $this->api = new ApiController();
        $this->viewData['locale'] = $this->locale;
    }

    public function index()
    {
        $this->setPathLanguages();

        $this->homepageData();

        $this->viewData['folders'] = Brochure::getBrochures($this->locale);

        return view('pages.home', $this->viewData);
    }

    /**
     * Set language for paths.
     */
    private function setPathLanguages() {
        /* @var \Mcamara\LaravelLocalization\LaravelLocalization $localisation */
        $localisation = app('laravellocalization');
        foreach ($localisation->getSupportedLocales() as $lang => $value) {
            $this->viewData['paths'][$lang] = '/' . $lang;
        }
    }

    /**
     * Get the data needed for the homepage from the Carrefour API.
     */
    private function homepageData()
    {
        $requests = [
            [
                'resource' => 'banners',
                'id' => 'homepage_banners',
                'query' => ['language' => $this->locale],
                'data_key' => 'banners',
            ],
            [
                'resource' => 'silos',
                'id' => null,
                'query' => ['language' => $this->locale],
                'data_key' => 'silos',
            ],
            [
                'resource' => 'homepage_content',
                'id' => null,
                'query' => ['language' => $this->locale],
                'data_key' => 'masonry_queue',
            ]
        ];

        foreach ($requests as $request) {
            try {
                $data = $this->api->getData($request['resource'], $request['id'], $request['query']);
                $this->viewData[$request['data_key']] = $data;
            }
            catch (RequestException $e) {}
        }
    }

}
