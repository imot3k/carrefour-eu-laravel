<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;

class RouteController extends Controller
{
    /**
     * @var string
     */
    private $locale;

    public function __construct()
    {
        $this->locale = App::getLocale();
    }

    public function index(Request $request)
    {
        $api = new ApiController();

        // Try to fetch a node from the API by path and language.
        $options = [
            'path' => str_replace("{$this->locale}/", '', $request->path()),
            'language' => $this->locale,
        ];
        $node = $api->getData('nodebypath', null, $options);

        // Continue only if the node is published in the CMS.
        if (!empty($node) && $node->status) {
            $this->flattenMetierContent($node);

            // Get the silos for constructing the menu.
            $silos = $api->getData('silos', null, [
                'language' => $this->locale,
                'exclude' => 'content',
            ]);

            $type = $node->type;

            JavaScript::put(['currentNode' => $node]);

            return view("pages.$type", ['node' => $node, 'silos' => $silos, 'locale' => $this->locale]);
        }

        // Get the silos for constructing the menu.
        $silos = $api->getData('silos', null, [
            'language' => $this->locale,
            'exclude' => 'content',
        ]);

        return response()->view('errors.404', ['silos' => $silos], 404);
    }

    /**
     * Flatten the content divided by Métier for the FQC page. Sort DESC on sticky and relevance date.
     *
     * @param \stdClass $node
     */
    private function flattenMetierContent(&$node)
    {
        if (property_exists($node, 'isMetierChapter') && $node->isMetierChapter == 1) {
            $flatContent = [];

            foreach ($node->content as $key => $content) {
                $flatContent = array_merge($flatContent, $content->content);
            }

            $flatContent = array_reverse(array_sort($flatContent, function($item) {
                return sprintf('%s,%s', $item->field_masonry_sticky, $item->field_date);
            }));

            $node->defaultContent = $flatContent;
        }
    }
}
