<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Psr\Http\Message\ResponseInterface;

class ApiController extends Controller
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var float
     */
    private $timeout = 15.0;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('API_URI'),
            'timeout' => $this->timeout,
        ]);
    }

    /**
     * Make a call to the API and return the JSON decoded response.
     *
     * @param string $resource
     *   The machine name of the resource.
     * @param null $id
     *   An optional ID to pass to the request URI.
     * @param array $query
     *   Optional query parameters that should be sent with the request.
     *
     * @return mixed
     * @throws RequestException
     */
    public function getData($resource, $id = NULL, $query = [])
    {
        try {
            $uri = $id ? "$resource/$id" : $resource;
            $request = $this->client->get($uri, ['query' => $query]);

            return json_decode($request->getBody());
        }
        catch (RequestException $e) {
            throw $e;
        }
    }

    /**
     * Make an asynchronous call to the API and return the JSON data.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function getAsyncData(Request $request)
    {
        $this->client = new Client([
            'base_uri' => env('API_URI'),
            'timeout' => $this->timeout,
        ]);

        $resource = $request->input('resource');
        $id = $request->input('id');
        $query = (array) $request->input('query');
        $template = $request->input('template');
		$viewData = (array) $request->input('viewData');
		$returnData = $request->input('returnData');

        // We expect at least a resource to be provided when making a call to the API.
        if (!$resource) {
            return response()->json([
                'code' => '400',
                'message' => 'Please provide a valid resource.',
            ], 400);
        }

        $uri = $id ? "$resource/$id" : $resource;
        $promise = $this->client->getAsync($uri, ['query' => $query]);
        $promise->then(
            function (ResponseInterface $response) use ($template, $returnData, $viewData) {
                // Return a rendered template if a template name has been provided. When no template has been provided
                // we just return the JSON response data.
				$body = $response->getBody();
				$viewData['content'] = json_decode($body);
                $output = $template ? view($template, ['data' => $viewData]) : $body;
				echo ($returnData == true ? json_encode(['data' => $viewData['content'], 'html' => $output->__toString()]) : $output);
            },
            function (RequestException $e) {
                echo $e->getMessage() . "\n";
                echo $e->getRequest()->getMethod();
            }
        );

        $promise->wait();
    }
}
