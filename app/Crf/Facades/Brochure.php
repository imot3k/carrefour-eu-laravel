<?php

namespace App\Crf\Facades;

use Illuminate\Support\Facades\Facade;

class Brochure extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'brochure';
    }
}