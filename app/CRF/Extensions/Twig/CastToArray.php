<?php
namespace App\Crf\Extensions\Twig;

use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Access Laravels form builder in your Twig templates.
 */
class CastToArray extends Twig_Extension
{
	/**
	 * Create a new extension
	 */
	public function __construct()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public function getName()
	{
		return 'TwigBridge_Extension_CastToArray';
	}

	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('castToArray', array($this, 'toArray')),
		);
	}

	public function toArray($object)
	{
		$return = [];
		if (is_object($object)) $return = get_object_vars($object);
		elseif (is_array($object)) $return = $object;
		return $return;
	}
}
?>