<?php

namespace App\Crf\Brochure;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;
use That0n3guy\Transliteration\Transliteration;
use Illuminate\Support\Facades\Log;

class Brochure
{
    /**
     * Fetches brochures for a given locale.
     *
     * @param string $locale
     *   Langcode, either NL or FR.
     *
     * @return array
     *   List of brochures.
     */
    public function getBrochures($locale)
    {
        $cacheKey = 'brochures_' . strtolower($locale);

        $brochures = Cache::remember($cacheKey, 6 * 60, function () use ($locale) {
            try {
                $brochures = $this->fetchAllBrochuresViaApi($locale);
                $brochures = $this->prepareData($brochures);
                $brochures = $this->fixData($brochures);
                $brochures = $this->prepareBrochuresForRendering($brochures);

                return $brochures;
            } catch (\Exception $e) { }

        });

        return $brochures;
    }

    /**
     * Creates a human-readable date interval for a brochure.
     *
     * @param stdClass $brochure
     *   The brochure that needs a date interval.
     *
     * @return string|FALSE
     *   A human-readable date interval, false if it could not be generated.
     */
    public function buildDateIntervalForDisplay(&$brochure)
    {
        $hasStartDate = (!empty($brochure->startDate));
        $hasEndDate = (!empty($brochure->endDate));

        // If we don't have a start date we bail out.
        if (!$hasStartDate) {
            return FALSE;
        }

        $startDate = $brochure->startDate;

        // If we don't have an end date we will also display the year. If we do
        // have an end date we will not show the year as part of the start date.
        $format = ($hasEndDate) ? 'd.m' : 'd.m.Y';

        $humanReadable = $startDate->format($format);

        if ($hasEndDate) {
            $endDate =  $brochure->endDate;

            $humanReadable .= ' - ' . $endDate->format('d.m.Y');
        }

        return $humanReadable;
    }

    /**
     * Fetches all brochures from the Carrefour API.
     *
     * @param string $locale
     *   The locale.
     *
     * @return array
     *   The brochures.
     */
    public function fetchAllBrochuresViaApi($locale)
    {
        // Fetch configuration.
        $apiEndpoint = config('brochure.api_endpoint');
        $apiMethod = config('brochure.api_method');
        $apiAuthentication = config('brochure.api_authentication');

        $brochures = array();

        // Make sure locale is uppercase.
        $locale = strtoupper($locale);

        $jar = new \GuzzleHttp\Cookie\CookieJar;

        try {

            $httpClient = new Client([
                'base_uri' => $apiEndpoint,
                // Needed because Carrefour uses self-signed certificates.
                'verify' => FALSE,
                'headers' => [
                    'autouser' => $apiAuthentication['autouser'],
                    'autokey' => $apiAuthentication['autokey'],
                    'Accept' => 'application/json',
                ],
            ]);

            $response = $httpClient->request('GET', $apiMethod,
                [
                    'query' => ['language' => $locale],
                    'cookies' => $jar,
                ]
            );

            $brochures = [];

            if ($response->getStatusCode() == 200) {
                $body = $response->getBody();

                $brochures = json_decode($body);

                if (!empty($brochures->folder)) {
                    $brochures = $brochures->folder;
                }
                else {
                    Log::error('Fetching brochures did not have a valid data structure. Fetching brochures aborted');
                }
            }
            else {
                Log::error('Fetching brochures did not return status 200. Fetching brochures aborted');
            }
        } catch(\Exception $e) {}

        return $brochures;
    }

    /**
     * Prepares the brochure data for usage in the templates.
     *
     * Also filters out brochures that do not have a thumbnail or do not have
     * a URL to view the brochure.
     *
     * @param array $brochures
     *   A list of raw brochure data, coming from the Carrefour API.
     *
     * @return array
     *   A list of brochure data, suited for usage in templates.
     */
    public function prepareBrochuresForRendering($brochures)
    {
        $validbrochures = [];

        foreach ($brochures as &$brochure) {
            if (is_object($brochure)) {
                $brochure->timespan = $this->buildDateIntervalForDisplay($brochure);
                $transliteration = new Transliteration();
                if (!empty($brochure->type)) {
                    $brochure->logoClass = strtolower($transliteration->clean_filename($brochure->type));
                }

                if (self::brochureIsActive($brochure) && !empty($brochure->thumbnailURL) && !empty($brochure->folderViewerURL)) {
                    $validbrochures[] = $brochure;
                }
            }
        }

        // Sort the brochures.
        usort($validbrochures, array($this, 'sortByPublicationDate'));

        // Limit to 20 brochures max.
        $validbrochures = array_slice($validbrochures, 0, 20);

        // Temporary workaround because the API data contains http URLs for the
        // thumbnails. This will be addressed in the CRF API in the near future.
        foreach ($validbrochures as &$brochure) {
            $brochure = self::convertThumbnailUrlToHttps($brochure);
        }

        return $validbrochures;
    }

    /**
     * Fix endDate always 1 year in the future for Simply You brochures.
     *
     * @param $brochures
     *   List of brochures.
     *
     * @return array
     *   List of brochures.
     */
    public function fixData($brochures) {
        foreach ((array) $brochures as &$brochure) {
            if ($brochure->type == 'SimplyYou') {

                // Exception for Simply You folders: when an enddate is more than
                // 2 months after the startdate, we limit the enddate to 2 months.
                $hasStartDate = (!empty($brochure->startDate));
                $hasEndDate = (!empty($brochure->endDate));

                // If we don't have a start or an end date we bail out.
                if (!$hasStartDate || !$hasEndDate) {
                    continue;
                }

                $startDate = $brochure->startDate;
                $endDate = $brochure->endDate;

                $twoMonths = clone $startDate;
                $twoMonths->add(new \DateInterval('P2M'));

                if ($endDate > $twoMonths) {
                    $brochure->endDate = $twoMonths;
                }
            }
        }

        return $brochures;
    }

    /**
     * Converts start- and enddate to DateTime objects.
     *
     * @param $brochures
     *   List of brochures.
     *
     * @return array
     *   List of brochures.
     */
    public function prepareData($brochures) {
        foreach ((array) $brochures as &$brochure) {
            $hasStartDate = (!empty($brochure->startDate));
            $hasEndDate = (!empty($brochure->endDate));

            if ($hasStartDate) {
                $brochure->startDate = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $brochure->startDate);
                $brochure->startDate->sub(new \DateInterval('P2D'));
            }

            if ($hasEndDate) {
                $brochure->endDate = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $brochure->endDate);
            }
        }

        return $brochures;
    }

    /**
     * Determines if a brochure is currently active.
     *
     * @param $brochure
     *   The brochure data.
     * @return
     *   True if published, false otherwise.
     */
    public function brochureIsActive($brochure) {
        $hasStartDate = (!empty($brochure->startDate));
        $hasEndDate = (!empty($brochure->endDate));

        // If we don't have a start date or an end date the brochure is
        // considered unpublished.
        if (!$hasStartDate || !$hasEndDate) {
            return FALSE;
        }

        $startDate = $brochure->startDate;
        $endDate = $brochure->endDate;
        $current_date = new \DateTime();

        return ($startDate <= $current_date && $endDate >= $current_date);
    }

    /**
     * Sort function for brochures.
     */
    function sortByPublicationDate($brochureA, $brochureB) {
        $startDateBrochureA = $brochureA->startDate;
        $startDateBrochureA = $startDateBrochureA->getTimestamp();
        $startDateBrochureB = $brochureB->startDate;
        $startDateBrochureB = $startDateBrochureB->getTimestamp();

        return ($startDateBrochureA < $startDateBrochureB);
    }

    /**
     * Convert thumbnail URLs from http to https.
     *
     * @param $brochure
     *   The brochure data.
     */
    function convertThumbnailUrlToHttps($brochure) {
        if (! empty($brochure->thumbnailURL)) {
            $brochure->thumbnailURL = str_replace('http://viewer.zmags', 'https://secure.viewer.zmags', $brochure->thumbnailURL);
        }

        return $brochure;
    }
}
