<?php

namespace App\Crf\Brochure\Providers;

use Illuminate\Support\ServiceProvider;
use App\Crf\Brochure\Brochure;

class BrochureServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $api = new Brochure();

        $this->app->instance('brochure', $api);
    }
}
