module.exports = function(grunt) {
	require('time-grunt')(grunt)
	require('jit-grunt')(grunt)
	//
	//
	// Project configuration.


	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		watch: {
			css:{
				files: "resources/assets/**/*.scss",
				tasks: ['sass:dist', 'cssmin']
			},
			js:{
				files: ["resources/assets/**/*.js"],
				tasks: ['browserify', 'uglify']
			}
		},
		sass: {
			options: {
				style: 'compact',
				sourceMap: true
			},
			dist: {
				files: {
					'public/css/cf.css': 'resources/assets/sass/cf.scss',
					'public/css/cf-print.css': 'resources/assets/sass/cf-print.scss',
					'public/css/cfeu.css': 'resources/assets/sass/cfeu.scss'
				}
			}
		},
		browserify: {
			/*options: {
				browserifyOptions : {
					debug: true
				}
			},*/
			dist: {
				files: {
					'public/js/bundle.js': ['resources/assets/js/main.js']
				}
			}
		},
		uglify: {
			dist: {
				options: {
					beautify: false
				},
				files: {
					'public/js/bundle.min.js': ['public/js/bundle.js']
				}
			}
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			dist: {
				files: {
					'public/css/cf.min.css': ['public/css/cf.css'],
					'public/css/cf-print.min.css': ['public/css/cf-print.css'],
					'public/css/cfeu.min.css': ['public/css/cfeu.css'],
				}
			}
		}
	});

	// Default task(s).
	grunt.registerTask('default', ['sass']);

	grunt.registerTask('build', ['sass', 'browserify', 'uglify', 'cssmin']);
	//grunt.registerTask('production', ['build', 'uglify', 'cssmin']);

	grunt.registerTask('build-webfont', 'Fetch webfont', function() {
		var done = this.async();

		var request = require('request'),
			fs = require('fs'),
			path = require('path');

		request('http://cdn-mkt.be.carrefour.eu/carrefour-icons/icons.css', function (error, response, body) {
			//grunt.file.write('public/css/font/icons.css', body);

			var myregexp = /\.(icon-[^:]+):before \{[\s\S]*?content: "([\s\S]*?)"/img;
			var match = myregexp.exec(body);
			var vars = '';
			while (match != null) {
				vars = vars + "\r\n$" + match[1] + ': "' + match[2] + '";';
				match = myregexp.exec(body);
			}
			grunt.file.write('resources/assets/sass/_icons.scss', vars);

			setTimeout(function() {done();}, 2000);
		});

		/*request('http://cdn-mkt.be.carrefour.eu/carrefour-icons/font/carrefour-sav.eot').pipe(fs.createWriteStream('public/css/font/carrefour-sav.eot'));
		request('http://cdn-mkt.be.carrefour.eu/carrefour-icons/font/carrefour-sav.woff').pipe(fs.createWriteStream('public/css/font/carrefour-sav.woff'));
		request('http://cdn-mkt.be.carrefour.eu/carrefour-icons/font/carrefour-sav.ttf').pipe(fs.createWriteStream('public/css/font/carrefour-sav.ttf'));
		request('http://cdn-mkt.be.carrefour.eu/carrefour-icons/font/carrefour-sav.svg').pipe(fs.createWriteStream('public/css/font/carrefour-sav.svg'));*/
	});
};
